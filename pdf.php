<?php

require_once('mydf.php');

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor(PDF_AUTHOR);
$pdf->SetTitle(PDF_TITLE);
$pdf->SetSubject(PDF_SUBJECT);
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();
$var = "VAR";

$X = $pdf->madina->marginLeft() + 2;
$RM = $pdf->madina->marginRight() + 2;
$pageW = $pdf->getPageWidth();
$contentW = $pageW - ($X + $RM);
$B0 = 0;
$BB = 'B0';
$B1 = 1;

$fontname = 'helvetica';
$fontstyle = '';
$fontsize = 8;
$pdf->SetFont($fontname, $fontstyle, $fontsize);
include('personalDetails.php');
include('generalExamination.php');
$tableWidth = ($pdf->getPageWidth() - ($pdf->madina->marginLeft() + $pdf->madina->marginRight()))/2;
$widthDiff = 15;
$tablewidth1 = $tableWidth - $widthDiff;
$tablewidth2 = $tableWidth + $widthDiff;

$labelY = $pdf->getY() + 1;
$tableSystematicW = $contentW * 0.4;
$tableLabel = " SYSTEMATIC EXAMINATION ";
$ExaminationW = $pdf->GetStringWidth($tableLabel,$fontname,$fontstyle,12) + 2;
$tableLabelX = $X + $tableWidth/2.5 - $ExaminationW/2;
$pdf->SetFont($fontname, $fontstyle, 12);
$pdf->SetTextColor(255,255,255);
$pdf->MultiCell($ExaminationW, 0, $tableLabel, $B0, 'C',true,1,$tableLabelX, $labelY);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($fontname, $fontstyle, $fontsize);

$tableY = $pdf->getY() + 1;
include('tableSystematic.php');
$pdf->writeHTMLCell($tableSystematicW,0,$X,$tableY,$tableSystematic,1);
$pdf->Ln();
$tableSystematicY = $pdf->getY();
$tableSystematicH = $tableSystematicY - $tableY;
// $pdf->setX($X);
// $pdf->cell($tableWidth,0,'hello',1,1);

$tableLabel = " LABORATORY EXAMINATION ";
$ExaminationW = $pdf->GetStringWidth($tableLabel,$fontname,$fontstyle,12) + 2;
$tableWidth = $contentW - $tableSystematicW;
$tableLabelX =  ($tableSystematicW + $X) + ($tableWidth/2) - ($ExaminationW/2);
$pdf->SetFont($fontname, $fontstyle, 12);
$pdf->SetTextColor(255,255,255);
$pdf->MultiCell($ExaminationW, 0, $tableLabel, $B0, 'C',true,1,$tableLabelX, $labelY);
$pdf->SetTextColor(0,0,0);

$pdf->SetFont($fontname, $fontstyle, 8);
include('tableLaboratory.php');
$pdf->writeHTMLCell($tableWidth,0,$tableSystematicW + $X,$tableY, $tableLaboratory,1);
$pdf->Ln();
$tableLaboratoryH = $pdf->getY() - $tableY;

$pdf->setX($X);
$txt = <<<EOD


Dear Sir/Madam,

This IS TO CERTIFY THAT MRS. UMA DAMAI is
CLINICALLY AND MENTALLY FIT AND THERE IS NO
EVIDENCE OF COMMUNICABLE DISEASE IN HER.

EOD;
$pdf->MultiCell($tableSystematicW,$tableLaboratoryH - $tableSystematicH,$txt,'LTR','J',false,1,$X,$tableSystematicY);
$txt = "Stamp of Health Care          Stamp & Signature of Physician
Organization";
$pdf->MultiCell($tableSystematicW,0,$txt,'LBR','J',false,0,$X);
$txt = <<<EOD

This Report is valid for Two months from the date of Medical Examination.
EOD;
$pdf->MultiCell($tableWidth,7,$txt,1,'C',false,1,$X + $tableSystematicW);
$pdf->Output(PDF_TITLE.'.pdf', 'i');
 ?>
