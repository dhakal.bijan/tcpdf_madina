<?php
// Include the main TCPDF library
require_once('./config/Madina.php');
require_once('./config/tcpdf.php');
require_once('./Madina.php');
require_once(__DIR__.'/vendor/tecnickcom/tcpdf/tcpdf.php');


/**
 *
 */
class MYPDF extends TCPDF
{

  public function __construct(){
    parent::__construct();
    $this->madina = new Madina();
    $this->DrawMargin();

  }

  public function DrawMargin(){
    $marginT = $marginR = $marginB = $marginL = $this->madina->marginLeft();
    // $marginT = $this->madina->marginTop();
    $this->SetLineStyle( array( 'width' => 0.2, 'color' => array(0, 0, 0)));
    //Top Line
    $this->Line($marginL, $marginT, $this->getPageWidth()-$marginR, $marginT);
    //RightLine
    $this->Line($this->getPageWidth()-$marginR, $marginT, $this->getPageWidth()-$marginR,  $this->getPageHeight()-$marginB);
    //bottom line
    $this->Line($marginL, $this->getPageHeight()-$marginB, $this->getPageWidth()-$marginR, $this->getPageHeight()-$marginB);
    //left Line
    $this->Line($marginL, $marginT, $marginL, $this->getPageHeight()-$marginB);
  }

  public function WaterMark(){
          $W = $this->w * 0.8;
          $X = $this->w/2 - $W/2;
          $Y = $this->h/2 - $W/2;
           // Logo
           $image_file = './logo.jpg';

           // get the current page break margin
           $bMargin = $this->getBreakMargin();
           // get current auto-page-break mode
           $auto_page_break = $this->AutoPageBreak;
           // disable auto-page-break
           $this->SetAutoPageBreak(false, 0);
           // set bacground image
           $this->Image($image_file, $X, $Y, $W, $W, '', '', '', false, 300, '', false, false, 0);
           // restore auto-page-break status
           $this->SetAutoPageBreak($auto_page_break, $bMargin);
           // set the starting point for the page content
           $this->setPageMark();

  }

  //Page header
   public function Header() {
     $this->DrawMargin();
     $this->WaterMark();
     $X = $this->madina->marginLeft() + 2;
     $Y = $this->madina->marginTop() + 2;
     $RM = $this->madina->marginRight() + 2;
     $B0 = 0;
     $B1 = 1;
     $logoW = $logoH = 25;
     $LeftW = 40;


     $image_file = './logo.jpg';
     $this->Image($image_file, $X, $Y, $logoW, $logoH, 'JPEG', '', '', false,'', '', false, false);
     $newY = $Y + $logoH * 1.2;


      //TITLE HEADING
      $font = array(
        'name'=>'helvetica',
        'style' =>'B',
        'size' => 15
      );
      $this->SetFont($font['name'], $font['style'], $font['size']);
      $this->MultiCell(0,0,PDF_HEADER_TITLE,$B0,'C',false,1,$X, $Y + 5,true,0,false,true,0,'T',false);

      $font = array(
        'name'=>'helvetica',
        'style' =>'',
        'size' => 6
      );
      $this->SetFont($font['name'], $font['style'], $font['size']);

      $text = '(An authorized medical center by the Government of Nepal)';
      $this->MultiCell(0,0,$text,$B0,'C',false,1,$X, $this->getY(),true,0,false,true,0,'T',false);
      $text = 'Kalimitidole, Kathmandu-9, Ph: +977-1-4479000';
      $this->MultiCell(0,0,$text,$B0,'C',false,1,$X, $this->getY(),true,0,false,true,0,'T',false);
      $text = 'E-mail: madinamedical2013@gmail.com';
      $this->MultiCell(0,0,$text,$B0,'C',false,1,$X, $this->getY(),true,0,false,true,0,'T',false);

      $Y = $imageY = max($this->getY(),$newY);
      $this->SetMargins(PDF_MARGIN_LEFT, $Y, PDF_MARGIN_RIGHT);
    }

   // Page footer disabled
   public function Footesr() {
       // Position at 15 mm from bottom
      //  $this->SetY(-15);
       // Set font
       $Y = $this->getY();
       $X = $this->getX();
       $RM = $this->madina->marginRight();
       $squareSize = 20;
       $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 1, 'color' => array(0, 0, 0));

       $this->Rect($X, $Y, $squareSize, $squareSize, 'D');
       $this->Line($X + $squareSize, $Y, $X, $Y + $squareSize, $style);
       $this->SetFont('helvetica', 'B', 12);
       $this->Text($X + 2,$Y + 4,'FIT',false,false,true,0,0,'L',0,'',0,true,'A','C');
       $this->Text($X + 5,$Y + 15,'UNFIT',false,false,true,0,0,'L',0,'',0,true,'A','C');
       $rect2x = $this->w - $RM - $squareSize;
       $this->Rect($rect2x, $Y, $squareSize, $squareSize, 'D');
       $this->Line($rect2x + $squareSize, $Y, $rect2x , $Y + $squareSize, $style);
       $this->Text($rect2x + 2, $Y + 4,'FIT',false,false,true,0,0,'L',0,'',0,true,'A','C');
       $this->Text($rect2x + 5, $Y + 15,'UNFIT',false,false,true,0,0,'L',0,'',0,true,'A','C');

       $this->SetFont('helvetica', 'B', 12);
       $textCellWidth = $this->w - 30 - $X - $RM ;
       $text = "This Report is valid for 2 months from the date of Medical Examination.";
       $this->MultiCell($textCellWidth,'',$text,0,'C',false,1,$X + 15, $Y + 3,true,0,false,true,0,'T',true);
       $text = 'Format-Recommended by Ministry of Health and Population Government of Nepal';
       $this->MultiCell($textCellWidth,'',$text,0,'C',false,1,$X + 15, $this->getY(),true,0,false,true,0,'T',true);
       $this->setY($Y + $squareSize + 1);
       $this->Line(0,$this->getY(),$this->w,$this->getY(),$style);

       $this->SetY(-7);

      $this->SetFont('helvetica', 'B', 12);
      $doctor = "Doctor's Signature";
      $techonlogist = "Technologist Signature";
      $dw = $this->GetStringWidth($doctor,'helvetica','B',12)+ 6;
      $tw = $this->GetStringWidth($techonlogist,'helvetica','B',12)+ 6;

      $y = $this->getY();
      $x = $this->madina->marginLeft();
      $this->MultiCell($dw,0,$doctor,'T','C',false,1,$x,$y,true,0,false,true,0,'T',true);
      $x = ($this->w - $this->madina->marginRight()- $tw);
      $this->MultiCell($tw,0,$techonlogist,'T','C',false,1,$x,$y,true,0,false,true,0,'T',true);
   }

}

 ?>
