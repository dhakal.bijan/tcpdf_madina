<?php

/**
* Document creator.
*/
define ('PDF_CREATOR', 'TCPDF');

/**
* Document author.
*/
define ('PDF_AUTHOR', 'MMC');

/**
* Header title.
*/

define ('PDF_HEADER_TITLE', 'Madina Medical Center Pvt. Ltd.');

/**
* Define title.
*/

define ('PDF_TITLE', 'Madina Medical Center');


/**
* Define title.
*/

define ('PDF_SUBJECT', 'Madina Medical Center SUBJECT');

/**
 * Header margin.
 */
define ('PDF_MARGIN_HEADER', 3);

/**
 * Footer margin.
 */
define ('PDF_MARGIN_FOOTER', 5);

/**
 * Top margin.
 */
define ('PDF_MARGIN_TOP', 30);

/**
 * Bottom margin.
 */
define ('PDF_MARGIN_BOTTOM', 5);

/**
 * Left margin.
 */
define ('PDF_MARGIN_LEFT', 1);

/**
 * Right margin.
 */
define ('PDF_MARGIN_RIGHT', 1);



?>
