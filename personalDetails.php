<?php
$X = $pdf->madina->marginLeft() + 5;
$Y = $pdf->getY() + 2;
$RM = $pdf->madina->marginRight() + 2;
$pageW = $pdf->getPageWidth();
$contentW = $pageW - ($X + $RM);
$B0 = 0;
$BB = 'B0';
$B1 = 1;
$imageY2 = 0;
$pdf->setXY($X,$Y);

function column($label,$value,$width){
  global $pdf;
  global $contentW;
  $labelWidth = $pdf->GetStringWidth($label.' :',$fontname,$fontstyle,$fontsize)+2;
  $valueWidth = $width - $labelWidth;
  $pdf->Cell($labelWidth,0,$label,1,0,'L');
  $pdf->Cell($valueWidth,0,$value,1,0,'L');

}

function title(){
  global $pdf, $X, $Y, $RM;
  $font = array(
    'name'=>'helvetica',
    'style' =>'',
    'size' => 7
  );
  $pdf->SetFont($font['name'], $font['style'], $font['size']);
  $sno = '1756 M';
  $textwidth = $pdf->GetStringWidth('S.NO:'. $sno, $font['name'], $font['style'], $fint['size']) + 3;
  $pdf->MultiCell($textwidth,0,'S.NO: '. $sno, $B0,'L',false,1,$pdf->getX(),$pdf->getY(),true,0,false,true,0,'T',false);

  //Middle Layout
  $SHW = $pdf->getPageWidth() - ($RM + $X);
  $pdf->SetFont('helvetica', 'B', 8);
  $text = "(Affiliated to Nepal Medical Occupational's Organization)";
  $textwidth = $pdf->GetStringWidth($text,'helvetica','B',10);
  $CenterX = $X + $SHW/2 - $textwidth/2;
  $pdf->MultiCell($textwidth,0,$text,0,'C',false,1,$CenterX,$subHeaddingY,false,0,false,true,0,'T',true);
  $subHeaddingY = $pdf->getY();

  $pdf->SetFont('helvetica', 'B', 11);
  $pdf->SetTextColor(255,255,255);
  $textwidth = $pdf->GetStringWidth(MADINA_REPORT,'helvetica', 'B', 11) + 5;
  $titleCenterX = $X + $SHW/2 - $textwidth/2;
  $pdf->MultiCell($textwidth,0,MADINA_REPORT,0,'C',true,1,$titleCenterX,$subHeaddingY,true,0,false,true,0,'T',true);
  $pdf->SetTextColor(0,0,0);

  $fit = "FIT";
  $textwidth = $pdf->GetStringWidth($fit,'helvetica', 'B', 11) + 5;
  $CenterX = $X + $SHW/2 - $textwidth/2;
  $pdf->MultiCell($textwidth,0,'FIT',1,'C',false,1,$CenterX,$pdf->getY() +1 ,true,0,false,true,0,'T',true);

  $pdf->setY($pdf->getY() + 5);
}
function imageFrame($width,$height){
  global $pdf, $X, $Y, $RM, $imageY2;
  $ImageW = $width;
  $ImageH = $height;
  $ImageX2 = $pdf->getPageWidth() - $RM;
  $ImageX1 = $ImageX2 - $ImageW;
  //PHOTO FRAME
  $style = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => array(0, 0, 0));

  $imageY1 = $Y;
  $imageY2 = $Y + $ImageH;
  //TopLine
  $pdf->Line($ImageX1, $imageY1, $ImageX2, $imageY1, $style);
  //bottom line
  $pdf->Line($ImageX1, $imageY2, $ImageX2, $imageY2, $style);
  //left line
  $pdf->Line($ImageX1, $imageY1, $ImageX1, $imageY2, $style);
  //rigth line
  $pdf->Line($ImageX2, $imageY1, $ImageX2, $imageY2, $style);
}

function personalDetails($width){
  global $pdf, $X;
  $pdf->SetFont('helvetica', 'B', 8);

  //form data

  $name = 'My Test Full Name';
  $age = '100';
  $sex = 'M';
  $martialStatus = 'Married';
  $passportNo = '0215649845698456';
  $examinationDate = "2017-12-12";
  $nationality = 'Nepali';
  $appliedCountry = 'MALAYSIA';
  $placeOfissue = "kammmakdmaskl";
  $dateofexpire = ' 2016-12-25';
  $recuritAgency = "Some agency Name";

  //first row
  //Name
  $pdf->setX($X);
  column('Name',$name,0.5 * $width);
  column('Age (Year)',$age,0.15 * $width);
  column('Sex ',$sex,0.1 * $width);
  column('Martial Status',$age,0.25 * $width);
  $pdf->Ln();
  $pdf->setX($X);
  column('Passport No.:',$passportNo,0.35 * $width);
  column('Expired On:',$dateofexpire,0.25 * $width);
  column('Passport issue place ',$placeOfissue,0.40 * $width);
  $pdf->Ln();
  $pdf->setX($X);
  column('Medical Examination Date :',$examinationDate,0.35 * $width);
  column('Applied Country:',$appliedCountry,0.35 * $width);
  column('Nationality ',$nationality,0.30 * $width);
  $pdf->Ln();
}
title();
imageFrame(30,35);
$width = $pdf->getPageWidth() - 30 - $X - $RM;
personalDetails($width - 2);

$Y = max($pdf->getY(),$imageY2) + 5;
$pdf->setY($Y);
?>
