<?php
$tableStart = '<table border="1">';
$tableEnd = '</table>';
$tableHeading = '<tr style="text-align:center;">';
$tableHeading .=  '<th  rowspan = "13" style="width:10%">'
.'<br><br><br>H<br>E<br>M<br>E<br>T<br>O<br>L<br>O<br>G<br>Y'
.'</th>';
  // . '<tcpdf method="StartTransform"/>'
  // . '<tcpdf method="Rotate" params="' . $pdf->serializeTCPDFtagParameters(array(90)) .'"/>'
  // . 'HEMETOLOGY'
  // . '<tcpdf method="StopTransform"/>'
  // .'</th>';
$tableHeading .= '<th style="width:40%">
Blood Examination
</th>
<th style="width:25%">
Results
</th>
<th style="width:25%">
Reference Range
</th>
</tr>
';
$result = "RESULT";
$tableData = array(
  'Total WBC Count' => ['results'=>$result,'range'=>'4000-11000'],
  'Differntial Count' => ['results'=>$result,'range'=>''],
  'Neutrophils' => ['results'=>$result,'range'=>'45-74%'],
  'Lymphocytes' => ['results'=>$result,'range'=>'25-40%'],
  'Eosinophils' => ['results'=>$result,'range'=>'1-6%'],
  'Monocytes' => ['results'=>$result,'range'=>'0-8%'],
  'Basophils' => ['results'=>$result,'range'=>'0-3%'],
  'ESR' => ['results'=>$result,'range'=>"M&lt;10, F&lt;20"],
  'Hemogiobin' => ['results'=>$result,'range'=>'M 12-17gm%'],
  ' ' => ['results'=>$result,'range'=>'F 11-14gm%'],
  'Malaria Parasite' => ['results'=>$result,'range'=>''],
  'Micro Filara' => ['results'=>$result,'range'=>''],
);

$tableRow = '';
foreach ($tableData as $key => $value) {
  $tableRow .= '
  <tr>
    <td>'.$key.'</td>
    <td>'.$value['results'].'</td>
    <td>'.$value['range'].'</td>
  </tr>';
};

$tableRow .= '
<tr style="text-align:center;">
  <td rowspan = "7" style="width:10%;font-size: 5.5pt;">
  <br>B<br>I<br>O<br>C<br>H<br>E<br>M<br>I<br>S<br>T<br>R<br>Y<br>
  </td>
  <td></td><td></td><td></td>
</tr>
';
$tableData = array(
  'Random Blood Sugar' => ['results'=>$result,'range'=>'80-120mg%'],
  'Urea' => ['results'=>$result,'range'=>'20-45mg%'],
  'Creatinine' => ['results'=>$result,'range'=>'0.4-1mg%'],
  'Bilirubin (Total/Direct)' => ['results'=>$result,'range'=>'0.4-1mg%'],
  'SGPT' => ['results'=>$result,'range'=>'5-37 U/L'],
  'SGOT' => ['results'=>$result,'range'=>'3-45 U/L'],
);
foreach ($tableData as $key => $value) {
  $tableRow .= '
  <tr>
    <td>'.$key.'</td>
    <td>'.$value['results'].'</td>
    <td>'.$value['range'].'</td>
  </tr>';
};
$tableData['Anti-Hiv(1&2)']= 'somevalue';
$tableRow .= '<tr>
<td rowspan="6"></td>
<td rowspan="6"  style="width:25%">SEROLOGY</td>
<td  style="width:35%">Anti-Hiv(1&2)</td>
<td  style="width:30%">'.$tableData["Anti-Hiv(1&2)"].'</td>
</tr>';
$tableData = array(
  'HBs-Ag' => 'bdas',
  'Anti-HCV' => 'dasd',
  'VDRL/RPR' => 'ddd',
  'TPHA' =>'sdasdw',
  'Blood Group (ABO/Rh)' => 'dasdasd'
);
foreach ($tableData as $key => $value) {
  $tableRow .= '
  <tr>
    <td>'.$key.'</td>
    <td>'.$value.'</td>
  </tr>';
};


$tableData['RBC']= 'somevalue';
$tableRow .= '<tr>
<td rowspan="4"></td>
<td rowspan="4">URINE</td>
<td>RBC</td>
<td>'.$tableData["RBC"].'</td>
</tr>';
$tableData = array(
  'Pus Cell' => 'bdas',
  'Epithelial Cells' => 'dasd',
  'Pregency Test (if female)' => 'ddd',
);
foreach ($tableData as $key => $value) {
  $tableRow .= '
  <tr>
    <td>'.$key.'</td>
    <td>'.$value.'</td>
  </tr>';
};
$others = "Eg. Oplates";
$cannabies = 'somevale';
$Mantoux = "someval";
$tableRow .= '<tr>
<td></td>
<td>Others</td>
<td>'.$others.'</td>
<td></td>
</tr>';

$tableRow .= '<tr>
<td></td>
<td>Cannabies</td>
<td>'.$cannabies.'</td>
<td></td>
</tr>';

$tableRow .= '<tr>
<td></td>
<td>Mantoux Test</td>
<td>'.$Mantoux.'</td>
<td></td>
</tr>';

$tableRow .= '<tr>
<td></td>
<td>Others</td>
<td>'.'$others'.'</td>
<td></td>
</tr>';


$txt = "(Signature of Lab Technician)";
$sig = <<<EOD
<br>
<p align="right">..................................................</p>
<p align="right">$txt</p>
<br>
EOD;
$tableLaboratory = $tableStart . $tableHeading . $tableRow . $tableEnd . $sig;


 ?>
